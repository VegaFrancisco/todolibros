package cl.dci.ufro.TodoLibros.helper;

import cl.dci.ufro.TodoLibros.modelo.Libro;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class main {

    public static ArrayList<Libro> cadenas = new ArrayList<Libro>();
    public static ArrayList<String> palabras = new ArrayList<String>();
    public static ArrayList<Libro> encontradas = new ArrayList<Libro>();
    public static Scanner sc = new Scanner(System.in);
    public static int nencontradas = 0;

    public static void main(String[] args){
        Buscador b=new Buscador();

        List<Libro> libros=new ArrayList<>();
        Libro l=new Libro();
        Libro l2=new Libro();
        Libro l3=new Libro();
        l.setNombre("El tunel 454");
        l2.setNombre("El tunel sadad");
        l3.setNombre("sadad");
        libros.add(l);
        libros.add(l2);
        libros.add(l3);

        b.palabrasClaves(libros,"el tunel");
    }


    public static boolean busqueda() {
        String patron = String.join("|", palabras);
        for (Libro cadena: cadenas) {
            Pattern pattern = Pattern.compile("\\b(" + patron + ")\\b", Pattern.CASE_INSENSITIVE);

            if( ! cadena.getNombre().isEmpty()) encontradas.add(cadena);
        }
        if (encontradas.size() > 0) return true;
        return false;
    }
}
