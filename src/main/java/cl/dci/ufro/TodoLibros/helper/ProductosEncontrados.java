package cl.dci.ufro.TodoLibros.helper;

import cl.dci.ufro.TodoLibros.modelo.Libro;
import cl.dci.ufro.TodoLibros.modelo.Pagina;
import cl.dci.ufro.TodoLibros.servicios.PaginaServico;
import org.springframework.beans.factory.annotation.Autowired;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductosEncontrados {
    private AticoLibros aticoLibros;
    private BuscaLibre buscaLibre;

    public ProductosEncontrados() {
        aticoLibros=new AticoLibros("https://aticolibros.cl/","AticoLibros");
        buscaLibre=new BuscaLibre("https://www.buscalibre.cl/","BuscaLibre");
    }

    public List<Libro> librosAmbasPaginas(Pagina pag1, Pagina pag2){
        List<Libro> almcTodo=new ArrayList<>();
        List<Libro> almcAL=aticoLibros.loadContentPage();
        List<Libro> almcBL=buscaLibre.loadContentPage();
        try{
            for (Libro libro : almcAL) {
                libro.setPagina_id(pag1);
                almcTodo.add(libro);
            }
            for (Libro libro : almcBL) {
                libro.setPagina_id(pag2);
                almcTodo.add(libro);
            }
        }catch (Exception e){

        }



        Collections.sort(almcTodo);

        return almcTodo;
    }

}
