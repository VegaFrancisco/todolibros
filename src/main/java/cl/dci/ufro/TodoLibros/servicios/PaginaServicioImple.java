package cl.dci.ufro.TodoLibros.servicios;

import cl.dci.ufro.TodoLibros.modelo.Pagina;
import cl.dci.ufro.TodoLibros.repositorios.PaginaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaginaServicioImple implements PaginaServico {

    @Autowired
    private PaginaRepositorio paginaRepositorio;

    @Override
    public Pagina buscarPorId(Integer id) {
        return paginaRepositorio.findById(id).orElse(null);
    }
}
