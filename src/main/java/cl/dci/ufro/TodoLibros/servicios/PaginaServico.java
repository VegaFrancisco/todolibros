package cl.dci.ufro.TodoLibros.servicios;

import cl.dci.ufro.TodoLibros.modelo.Pagina;

public interface PaginaServico {
    public Pagina buscarPorId(Integer id);
}
