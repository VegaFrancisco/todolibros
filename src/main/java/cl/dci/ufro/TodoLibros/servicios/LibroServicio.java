package cl.dci.ufro.TodoLibros.servicios;

import cl.dci.ufro.TodoLibros.modelo.Libro;

import java.util.List;

public interface LibroServicio {
    public List<Libro> mostrarLibros();
    public void guardarLibro(Libro libro);
    public void eliminarCatalogo();
}
