package cl.dci.ufro.TodoLibros;

import cl.dci.ufro.TodoLibros.modelo.ERole;
import cl.dci.ufro.TodoLibros.modelo.Pagina;
import cl.dci.ufro.TodoLibros.modelo.Role;
import cl.dci.ufro.TodoLibros.repositorios.PaginaRepositorio;
import cl.dci.ufro.TodoLibros.repositorios.RoleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Precarga {



    @Bean(name = "pagina")
    CommandLineRunner cargaPaginas(PaginaRepositorio paginaRepositorio){
        return(args->{
            paginaRepositorio.save(new Pagina("AticoLibros"));
            paginaRepositorio.save(new Pagina("BuscaLibre"));
        });
    }

    @Bean(name = "roles")
    CommandLineRunner cargaRoles(RoleRepository rolRepositorio){
        return(args->{
            rolRepositorio.save(new Role(ERole.ROLE_USER));
            rolRepositorio.save(new Role(ERole.ROLE_MODERATOR));
            rolRepositorio.save(new Role(ERole.ROLE_ADMIN));
        });
    }


}
