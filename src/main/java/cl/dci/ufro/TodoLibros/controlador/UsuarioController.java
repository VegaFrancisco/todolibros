package cl.dci.ufro.TodoLibros.controlador;

import cl.dci.ufro.TodoLibros.modelo.User;
import cl.dci.ufro.TodoLibros.repositorios.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {
    @Autowired
    UserRepository userRepository;

    @GetMapping("/byId/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public User getById(@PathVariable Long id){
        return userRepository.findById(id).orElse(null);
    }

}
