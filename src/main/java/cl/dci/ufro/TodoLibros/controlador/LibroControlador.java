package cl.dci.ufro.TodoLibros.controlador;

import cl.dci.ufro.TodoLibros.helper.Buscador;
import cl.dci.ufro.TodoLibros.helper.ProductosEncontrados;
import cl.dci.ufro.TodoLibros.modelo.Libro;
import cl.dci.ufro.TodoLibros.servicios.LibroServicio;
import cl.dci.ufro.TodoLibros.servicios.PaginaServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/libros")
public class LibroControlador {
    @Autowired
    private LibroServicio libroServicio;

    @Autowired
    private PaginaServico paginaServico;

    private ProductosEncontrados productosEncontrados=new ProductosEncontrados();

    @GetMapping("/mostrarLibros")
    public List<Libro> libros(){
        return libroServicio.mostrarLibros();
    }

    @GetMapping("/agregarLibros")
    public String agregarLibros(){
        libroServicio.eliminarCatalogo();
        List<Libro> libros=productosEncontrados.librosAmbasPaginas(paginaServico.buscarPorId(1),paginaServico.buscarPorId(2));
        for(Libro l:libros){
            libroServicio.guardarLibro(l);
        }
        return "Listo";
    }

    @GetMapping("/eliminarLibros")
    public String eliminarCatalogo(){
        libroServicio.eliminarCatalogo();
        return "Listo";
    }

    @GetMapping("/buscarUnLibro/{palabra}")
    public List<Libro> buscarLibro(@PathVariable String palabra){
        Buscador b=new Buscador();
        return b.palabrasClaves(libroServicio.mostrarLibros(),palabra);
    }

}
