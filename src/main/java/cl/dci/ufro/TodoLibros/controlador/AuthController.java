package cl.dci.ufro.TodoLibros.controlador;

import cl.dci.ufro.TodoLibros.modelo.Account;
import cl.dci.ufro.TodoLibros.modelo.ERole;
import cl.dci.ufro.TodoLibros.modelo.Role;
import cl.dci.ufro.TodoLibros.modelo.User;
import cl.dci.ufro.TodoLibros.payload.request.LoginRequest;
import cl.dci.ufro.TodoLibros.payload.request.SignupRequest;
import cl.dci.ufro.TodoLibros.payload.request.SignupRequest3;
import cl.dci.ufro.TodoLibros.payload.response.JwtResponse;
import cl.dci.ufro.TodoLibros.payload.response.MessageResponse;
import cl.dci.ufro.TodoLibros.repositorios.AccountRepository;
import cl.dci.ufro.TodoLibros.repositorios.RoleRepository;
import cl.dci.ufro.TodoLibros.repositorios.UserRepository;
import cl.dci.ufro.TodoLibros.security.jwt.JwtUtils;
import cl.dci.ufro.TodoLibros.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt,
												 userDetails.getId(), 
												 userDetails.getUsername(), 
												 userDetails.getEmail(), 
												 roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUserrr(@Valid @RequestBody SignupRequest3 signUpRequest) {

		if (accountRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}
		if (userRepository.existsByNombreUsuario(signUpRequest.getNombreUsuario())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: NombreUsuario is already in use!"));
		}

		//Create new user
		User user=new User(signUpRequest.getNombreUsuario());

		// Create new user's account
		Account account = new Account(signUpRequest.getNombreUsuario(),
				signUpRequest.getEmail(),
				encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
					case "admin":
						Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(adminRole);

						break;
					case "mod":
						Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(modRole);

						break;
					default:
						Role userRole = roleRepository.findByName(ERole.ROLE_USER)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(userRole);
				}
			});
		}



		account.setRoles(roles);

		user.setAccount(accountRepository.findById(accountRepository.save(account).getId()).get());
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}


}
