package cl.dci.ufro.TodoLibros.modelo;

/**
 *
 * @author Miguel Ortiz
 */
public enum ERole {
	ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
