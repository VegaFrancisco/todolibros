package cl.dci.ufro.TodoLibros.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(	name = "libro")
public class Libro implements Comparable<Libro>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String nombre;
    private String autor;
    private String editorial;
    private String categoria;
    private String idioma;
    private int stock;
    private int precioAhora;
    private int precioAntes;
    private int porcientoDescuento;
    private String urlCompra;
    private String urlImg;
    @Size(max = 2000)
    private String resena;

    @ManyToOne
    @JoinColumn(name = "pagina_id")
    @JsonIgnoreProperties("libros")
    private Pagina pagina_id;

    public Libro() {
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getPrecioAhora() {
        return precioAhora;
    }

    public void setPrecioAhora(int precioAhora) {
        this.precioAhora = precioAhora;
    }

    public int getPrecioAntes() {
        return precioAntes;
    }

    public void setPrecioAntes(int precioAntes) {
        this.precioAntes = precioAntes;
    }

    public int getPorcientoDescuento() {
        return porcientoDescuento;
    }

    public void setPorcientoDescuento(int porcientoDescuento) {
        this.porcientoDescuento = porcientoDescuento;
    }

    public String getUrlCompra() {
        return urlCompra;
    }

    public void setUrlCompra(String urlCompra) {
        this.urlCompra = urlCompra;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getResena() {
        return resena;
    }

    public void setResena(String resena) {
        this.resena = resena;
    }

    public Pagina getPagina_id() {
        return pagina_id;
    }

    public void setPagina_id(Pagina pagina_id) {
        this.pagina_id = pagina_id;
    }

    @Override
    public int compareTo(Libro o) {
        if (o.getPrecioAhora() > precioAhora) {
            return -1;
        } else if (o.getPrecioAhora() > precioAhora) {
            return 0;
        } else {
            return 1;
        }
    }
}
