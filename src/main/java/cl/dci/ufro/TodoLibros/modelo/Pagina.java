package cl.dci.ufro.TodoLibros.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Table(	name = "pagina")
public class Pagina {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "Nombre de la pagina requerido")
    private String nombre;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pagina_id")
    @JsonIgnoreProperties("pagina")
    private Set<Libro> libros;

    public Pagina() {
    }

    public Pagina(@NotBlank(message = "Nombre de la pagina requerido") String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Libro> getLibros() {
        return libros;
    }

    public void setLibros(Set<Libro> libros) {
        this.libros = libros;
    }
}
