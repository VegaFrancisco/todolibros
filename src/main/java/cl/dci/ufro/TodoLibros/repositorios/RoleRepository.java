package cl.dci.ufro.TodoLibros.repositorios;

import cl.dci.ufro.TodoLibros.modelo.ERole;
import cl.dci.ufro.TodoLibros.modelo.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(ERole name);
}
