package cl.dci.ufro.TodoLibros.repositorios;

import cl.dci.ufro.TodoLibros.modelo.Libro;
import cl.dci.ufro.TodoLibros.modelo.Pagina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaginaRepositorio extends JpaRepository<Pagina,Integer> {
}
