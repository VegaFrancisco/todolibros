package cl.dci.ufro.TodoLibros.repositorios;

import cl.dci.ufro.TodoLibros.modelo.Account;
import cl.dci.ufro.TodoLibros.modelo.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Boolean existsByNombreUsuario(String usuario);

}
